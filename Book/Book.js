export default {

    name: 'Book',
    props: {  
        data: {
          type: Object,
          required: false
        }
    },
    template: `
        <article class="article">
            <div class="col col--cbx"><input type="checkbox" checked="checked" /></div>
            <div class="col">{{ data.code }}</div>
            <div class="col col--main"><h2 class="title-2">{{ data.title }}</h2></div>
            <div class="col"><h3 class="title-3">{{ data.author }}</h3></div>
            <div class="col"><time :datetime="data.createdAt">{{ formatDate(data.createdAt) }}</time></div>
            <div class="col">{{ formatPrice(data.price) }}</div>
        </article>
    `,
    methods: {      

        formatDate(dateISO) {
            return new Date(dateISO).toLocaleDateString("fr-FR")
        },

        formatPrice(price) {
            return Number(price).toFixed(2) + ' €'
        }
    }
};