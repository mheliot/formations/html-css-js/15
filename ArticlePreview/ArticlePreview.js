export default {

    name: 'ArticlePreview',

    data() {
        return {
            isActive: false
        }
    },

    methods: {

        hide: function() {
            this.isActive = false
        },

        show: function() {
            this.isActive = true
        }
    },

    template: `
        <aside class="article-preview" :class="{ 'active': isActive }">
            <div class="article-preview__content">
                <slot></slot>
            </div>
            <div class="article-preview__actions">
                <button class="button" type="button" @click="hide()">Fermer</button>
            </div>
        </aside>
    `
};